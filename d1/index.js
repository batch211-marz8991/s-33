// console.log("Hello Wolrd")

//javascript synchronous and asyncronous

/*
	synchronous codes runs in sequence. this means that the operation must wait for the previous one to complete before exucuting.


	asynchronous code execution is often preferable in  situation where execution can be blocked. some examples of this are network request, long-running calculation, file system operation, ect. Using asynchronous code in the browser ensures the page remains response and the users experience is mostly unaffected




*/


console.log("Hello World");
// conole.log("Hello World");

// for (let i = 0; i <= 100; i++) (
// 	console.log(i)
// 	);

// console.log("Hello it's me");


/*
	API stands for Application programming interface
	-API is a particular set of codes that allows softwawre program to communicate with each other 
	-API is the interface through which you access someone else's code or through which someone else's code accesses yours.

	Example:
		Google API
			https://developers.google.com/identity/sign-in/web/sign-in
		Youtube API
			https://developers.google.com/youtube/iframe_api_reference
*/

//Fetch API


// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

/*
	A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

	A promise is in one of those three states:
	Pending:
		-Initial state, neigther fulfilled nor rejected
	fulfilled:
		-Operation was successfully completed
	Rejected:
		-Operation failed

*/

// by using the .then method, we can now check for the status of promise
// the fetch method will return a promise taht resolves to be a response object
// the .then method capyures the response object and returns another promise which eventually be resolved or rejected 
// syntax
//			fetch('').then(response => ())
// fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status))

// fetch('https://jsonplaceholder.typicode.com/posts')
// .then(response => response.json())
// .then((json) => console.log(json))

/*
	- the "async" and "await" keyword is another approach that can be used to achieved asynchronous codes
	- used 
*/

// async function fetchData () {
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
// 	console.log(result);
// 	console.log(typeof result);

// 	console.log(result.body)
// 	let json = await result.json();
// 	console.log(json)
// };
// fetchData();

//GETTING A SPECIFIC POST
//Retrrieve a specific post following the rest API (retrieve, /post/id:, GET)

// fetch('https://jsonplaceholder.typicode.com/posts/1')
// .then(response => response.json())
// .then((json) => console.log(json))

/*
	POSTMAN

	url: https://jsonplaceholder.typicode.com/posts/1
	method: GET

*/

/*
	CREATING A POST
	syntax:
		fetch('URL', option)
		.then((response) => {})
		.then((response) => {})
*/

//create a new post following the REST API (create, /post/:id, POST)
	// fetch('https://jsonplaceholder.typicode.com/posts', {
	// 	method: 'POST',
	// 	header: {
	// 		'Content-Type': 'application/json'
	// 	},
	// 	body: JSON.stringify({
	// 		tittle: 'New post',
	// 		body: 'Hello World',
	// 		userId: 1
	// 	})
	// })
	// .then(response => response.json())
	// .then((json) => console.log(json))

//Updating a post

//updates a specific post following the rest api

// fetch('https://jsonplaceholder.typicode.com/posts/1', {
// 	method: 'PUT',
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		id: 1,
// 		tittle: 'Update post',
// 		body: 'Hello again',
// 		userId: 1
// 	})
// })
// .then(response => response.json())
// 	.then((json) => console.log(json))

	/*
		POSTMAN

		url: https://jsonplaceholder.typicode.com/posts/1
		method: PUT
		body: raw + json
			{
				"tittle": "My First Revised Blog Post"
				"body": "Hello there! I revised this a bit.",
				"userId": 1
			}


	*/

	//update a post using a PATCH

	// fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// 	method: 'PATCH',
	// 	headers: {
	// 	'Content-Type': 'application/json'
	// 	},
	// 	body: JSON.stringify({
	// 	tittle: 'Corrected post',
	// 	})
	// })
	// .then(response => response.json())
	// .then((json) => console.log(json))
//
//		url: https://jsonplaceholder.typicode.com/posts/1
//		method: PATCH
//		body: raw + json
//			{
//				"tittle": "This is my final tittle"
//			}
//


//DELETING A SPECIFIC POST USING REST API

// fetch("https://jsonplaceholder.typicode.com/posts/1", {
// 		method: 'DELETE'
// 	})
	

	//FILTERING THE POST
	/*
		The data can be filtered by sending the userId along with the URL
		- information sent via the URL can be done by adding the question marksymbol
	*/
// fetch("https://jsonplaceholder.typicode.com/posts/1")
// .then(response => response.json())
// .then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3")
.then(response => response.json())
.then((json) => console.log(json))


//Retrieve comments of a specific post
// retrieving a specific post using rest api


fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json)); 