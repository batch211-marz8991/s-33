fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))


//getting all to do list items
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.tittle;
	}))
	console.log(list);
})

// getting specific to do list item

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.tittle}" on the list has a status of ${json.completed}`))


//create a todo list item using post method

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		tittle: 'Created To Do List Item',
		completed: false, 
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// update a to do list using fetch method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		tittle: "Update To do List Item",
		description: "To update my to do list with a different data structure",
		status: "Pending",
		dateCompleted: 'Pending',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


//updating a to do list item using PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
		'Content-Type': 'application/json'
		},
		body: JSON.stringify({
		status: 'completed',
		dateCompleted: '07/09/21'
	})
	})
	.then(response => response.json())
	.then((json) => console.log(json))

	//delete
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'DELETE'
	})